Soal 1 Membuat Database

CREATE DATABASE myshop;

Soal 2 Membuat Table di Dalam Database

-MEMBUAT TABEL USERS

CREATE TABLE users( 
id int(8) PRIMARY KEY AUTO_INCREMENT, 
name varchar(255) NOT null, 
email varchar(255) NOT null, 
password varchar(255) NOT null 
);

-MEMBUAT TABEL CATEGORIES

CREATE TABLE categories( 
id int(8) PRIMARY KEY AUTO_INCREMENT, 
name varchar(255) NOT null 
);

-MEMBUAT TABEL ITEMS

CREATE TABLE items( 
id int(8) PRIMARY KEY AUTO_INCREMENT, 
name varchar(255) NOT null, 
description varchar(255) NOT null, 
price int(18) NOT null, stock int(8) NOT null, 
category_id int(8), 
FOREIGN KEY (category_id) REFERENCES categories(id) 
);


Soal 3 Memasukkan Data pada Table

-MENGISI TABEL USERS

INSERT INTO users (name, email, password) 
values 
('John Doe','john@doe.com','john123'), 
('Jane Doe','jane@doe.com','jenita123');

-MENGISI TABEL CATEGORIES

INSERT INTO categories(name) 
VALUES 
('gadget'), ('cloth'), ('men'), ('women'), ('branded');

-MENGISI TABEL ITEMS

INSERT INTO items(name, description, price, stock, category_id) 
VALUES 
('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1), 
('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2), 
('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);


Soal 4 Mengambil Data dari Database

a. Mengambil data users

SELECT id, name, email FROM users;

b. Mengambil data items

-DENGAN KONDISI HARGA LEBIH DARi 1.000.000

SELECT * FROM items WHERE price > 1000000;

-DENGAN KONDISI NAMA MENGANDUNG KATA "WATCH"

SELECT * FROM items WHERE name LIKE "%watch";

-DENGAN KONDISI MENAMPILKAN KATEGORI DI MASING2 ITEMS (JOIN)

SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM items LEFT JOIN categories on items.category_id = categories.id;


Soal 5 Mengubah Data dari Database

-MENGUBAH DATA ITEMS DENGAN NAMA "sumsang b50" DENGAN HARGA 4000000 MENJADI 2500000

UPDATE items set price = 2500000 where id = 1;
